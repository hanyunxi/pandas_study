import pandas as pd

'''
数据转换函数对比：map、apply、applymap：

map：只用于Series，实现每个值->值的映射；
apply：用于Series实现每个值的处理，用于Dataframe实现某个轴的Series的处理；
applymap：只能用于DataFrame，用于处理该DataFrame的每个元素；
'''
# 公司股票代码到中文的映射，注意这里是小写
dict_company_names = {
    "bidu": "百度",
    "baba": "阿里巴巴",
    "iq": "爱奇艺",
    "jd": "京东"
}

def data_convert():
    # 1. map用于Series值的转换¶
    # 实例：将股票代码英文转换成中文名字
    # Series.map(dict) or Series.map(function)均可
    filepath = "../datas/stocks/互联网公司股票.xlsx"
    df = pd.read_excel(filepath)
    print(df.head())
    print(df['公司'].unique())
    print("----------------------------------------------------------------")
    # 方法1：Series.map(dict)¶
    df['中文名称'] = df['公司'].str.lower().map(dict_company_names)
    print("----------------------------------------------------------------")
    df['中文名称2'] = df['公司'].map(lambda x : dict_company_names[x.lower()])
    print(df.head(10))

    # 2. apply用于Series和DataFrame的转换
    # Series.apply(function), 函数的参数是每个值
    # DataFrame.apply(function), 函数的参数是Series
    df['中文名称3'] = df['公司'].apply(lambda x : dict_company_names[x.lower()])
    # DataFrame.apply(function)¶  function的参数是对应轴的Series
    df['中文名称4'] = df.apply(lambda x : dict_company_names[x["公司"].lower()],axis=1)

    print(df.head())

    # 3. applymap用于DataFrame所有值的转换
    sub_df = df[['收盘', '开盘', '高', '低', '交易量']]
    print(sub_df)
    print("------------------------------------------------------")
    # 将这些数字取整数，应用于所有元素
    print(sub_df.applymap(lambda x: int(x)))
    print("------------------------------------------------------")
    # 直接修改原df的这几列
    df.loc[:, ['收盘', '开盘', '高', '低', '交易量']] = sub_df.applymap(lambda x : int(x))

    print(df)






if __name__ == '__main__':
    data_convert()