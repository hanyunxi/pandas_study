import pandas as pd

def read_data():
    filepath = '../datas/beijing_tianqi/beijing_tianqi_2018.csv'
    df = pd.read_csv(filepath)
    # 设定索引为日期，方便按日期筛选
    df.set_index('ymd',inplace=True)
    # head() 默认前5
    # print(df.head(10))
    # 替换掉温度的后缀℃
    df.loc[:,'bWendu'] = df['bWendu'].str.replace("℃","").astype('int32')
    df.loc[:,'yWendu'] = df['yWendu'].str.replace("℃","").astype('int32')
    # print(df.dtypes)
    # print(df.head())
    # 1、使用单个label值查询数据
    # 行或者列，都可以只传入单个值，实现精确匹配
    # 得到单个值
    print(df.loc['2018-01-05', 'bWendu'])
    # 得到一个Series
    print(df.loc['2018-01-05', ['bWendu', 'yWendu']])
    # 2、使用值列表批量查询
    # 得到Series  或 得到DataFrame
    print(df.loc[['2018-01-03', '2018-01-04', '2018-01-05'], ['bWendu','yWendu']])
    print("---------------------------------------------------------------------")
    # 3、使用数值区间进行范围查询
    # 行index按区间
    print(df.loc['2018-01-03':'2018-01-10', ['bWendu', 'yWendu','fengxiang']])
    print("---------------------------------------------------------------------")
    print(df.loc['2018-01-09', 'bWendu':'fengxiang'])
    print("---------------------------------------------------------------------")
    # bool列表的长度得等于行数或者列数
    # 4、使用条件表达式查询
    # 简单条件查询，最低温度低于-10度的列表
    print(df.loc[df['yWendu'] <= 10, :])

    # 复杂条件查询，查一下我心中的完美天气
    # 注意，组合条件用&符号合并，每个条件判断都得带括号
    ## 查询最高温度小于30度，并且最低温度大于15度，并且是晴天，并且天气为优的数据
    # print(df.head())
    print("-----------------------------------------------------------------------")
    print(df.loc[(df['bWendu'] < 30) & (df['yWendu'] > 15) & (df['tianqi'] == '晴') & (df['aqiLevel'] == 1),:])

    # 5、调用函数查询¶
    # 直接写lambda表达式
    print(df.loc[lambda df: (df['bWendu'] < 30) & (df['yWendu'] > 15) & (df['tianqi'] == '晴') & (df['aqiLevel'] == 1), :])


# 编写自己的函数，查询9月份，空气质量好的数据
def query_air(df):
    return df.index.str.startswith("2018-09") & (df["aqiLevel"]==1)



if __name__ == '__main__':
    read_data()
    filepath = '../datas/beijing_tianqi/beijing_tianqi_2018.csv'
    df = pd.read_csv(filepath)
    df.set_index('ymd',inplace=True)
    print(df.loc[query_air, :])