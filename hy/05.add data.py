import pandas as pd


def adddata():
    filepath = "../datas/beijing_tianqi/beijing_tianqi_2018.csv"
    df = pd.read_csv(filepath)
    # print(df.head())
    # 1、直接赋值的方法¶ 实例：清理温度列，变成数字类型
    df.loc[:, 'bWendu'] = df['bWendu'].str.replace("℃", "").astype('int32')
    df.loc[:, 'yWendu'] = df['yWendu'].str.replace("℃", "").astype('int32')
    # 注意，df["bWendu"]其实是一个Series，后面的减法返回的是Series
    df.loc[:, 'wencha'] = df['bWendu'] - df['yWendu']
    print(df.head())
    # 注意需要设置axis==1，这是series的index是columns
    # axis = 1  增加一列  axis = 0 增加一行
    # 2、df.apply方法¶
    # 实例：添加一列温度类型：
    # 如果最高温度大于33度就是高温
    # 低于-10度是低温
    # 否则是常温
    df.loc[:, 'wencha_type'] = df.apply(get_wencha_type, axis=1)
    print(df.head())
    print("-----------------------------------------------------------------")
    # 查看温度类型的计数
    print(df['wencha_type'].value_counts())
    print("-----------------------------------------------------------------")
    # 3、df.assign方法
    # Assign new columns to a DataFrame.
    # Returns a new object with all original columns in addition to new ones.
    # 可以同时添加多个新的列
    cp = df.assign(
        yWendu_huashi=lambda x : x["yWendu"] * 9 / 5 + 32,
        # 摄氏度转华氏度
        bWendu_huashi=lambda x : x["bWendu"] * 9 / 5 + 32
    )
    # df.assign(yWendu_huashi=lambda x: x["yWendu"] * 9 / 5 + 32).assign(bWendu_huashi=lambda x: x["bWendu"] * 9 / 5 + 32)
    print(cp)
    # 4、按条件选择分组分别赋值
    # 按条件先选择数据，然后对这部分数据赋值新列
    # 实例：高低温差大于10度，则认为温差大
    df['wencha_type2'] = ''
    df.loc[df['bWendu'] - df['yWendu'] > 10 ,'wencha_type2'] = '温差大'
    df.loc[df['bWendu'] - df['yWendu'] <= 10 ,'wencha_type2'] = '温差小'
    # print(df.head())

def get_wencha_type(df):
    if df['bWendu'] > 33:
        return '高温'
    if df['yWendu'] < -10:
        return '低温'
    return '常温'


if __name__ == '__main__':
    adddata()
