import pandas as pd
import pymysql


def read_csv():
    df = pd.read_csv('../datas/ml-latest-small/ratings.csv')
    # 查看前几行数据   head() 不传参  默认前5条
    print(df.head())
    # 查看数据的形状，返回(行数、列数)
    print(df.shape)
    # 查看列名列表
    print(df.columns)
    # 查看索引列
    print(df.index)
    # 查看每列的数据类型
    print(df.dtypes)

def read_txt():
    filepath = '../datas/crazyant/access_pvuv.txt'
    df = pd.read_csv(
        filepath,
        sep="\t",
        header=None,
        names=['pdate','pv','uv']
    )
    print(df)
    print(df.shape)

def read_excel():
    filepath = '../datas/crazyant/access_pvuv.xlsx'
    df = pd.read_excel(filepath)
    print(df)

def read_mysql():
    conn = pymysql.connect(host='192.168.1.99',port=3306,user='root',password='1',database='SJAnti-fraud')
    df = pd.read_sql("select * from dissuade_dept",con=conn)
    print(df)


if __name__ == '__main__':
    # read_csv()
    # read_txt()
    # read_excel()
    read_mysql()