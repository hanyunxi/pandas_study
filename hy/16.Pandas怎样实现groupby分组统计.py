import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
'''
类似SQL：
select city,max(temperature) from city_weather group by city;

groupby：先对数据分组，然后在每个分组上应用聚合函数、转换函数
'''
# pip3 install matplotlib


df = pd.DataFrame({'A': ['foo', 'bar', 'foo', 'bar', 'foo', 'bar', 'foo', 'foo'],
                   'B': ['one', 'one', 'two', 'three', 'two', 'two', 'one', 'three'],
                   'C': np.random.randn(8),
                   'D': np.random.randn(8)})
def group_demo():
    # matplotlib.matplotlib_fname()
    # 一、分组使用聚合函数做数据统计
    # 我们看到：
    #
    # groupby中的'A'变成了数据的索引列
    # 因为要统计sum，但B列不是数字，所以被自动忽略掉
    print(df.groupby('A').sum())
    # print(df)

    # 2、多个列groupby，查询所有数据列的统计¶
    print(df.groupby(['A','B']).mean())
    print("------------------------------------------------")
    # 我们看到：('A','B')成对变成了二级索引
    print(df.groupby(['A','B'],as_index=False).mean())

    print("------------------------------------------------")
    # 3、同时查看多种数据统计
    # 方法1：预过滤，性能更好
    print(df.groupby('A')['C'].agg([np.sum, np.mean, np.std]))
    # 我们看到：列变成了多级索引
    print("------------------------------------------------")
    # 方法2
    print(df.groupby('A').agg([np.sum, np.mean, np.std])['C'])

    # 4、查看单列的结果数据统计
    print(df.groupby('A').agg({"C": np.sum, "D": np.mean}))

# 二、遍历groupby的结果理解执行流程
def group_execute():
    g = df.groupby('A')
    for name,group in g:
        print(name)
        print(group)
        print()
    # 可以获取单个分组的数据
    print(g.get_group("bar"))
    print("----------------------------------------------")
    # 2、遍历多个列聚合的分组
    g2 = df.groupby(['A','B'])
    # 可以看到，name是一个2个元素的tuple，代表不同的列
    for name,group in g2:
        print(name)
        print(group)
        print()

    print("----------------------------------------------")
    print(g2.get_group(('foo', 'one')))

    # 可以直接查询group后的某几列，生成Series或者子DataFrame
    for name, group in g['C']:
        print(name)
        print(group)
        print(type(group))
        print()

# 三、实例分组探索天气数据

def group_tianqi():
    filepath = "../datas/beijing_tianqi/beijing_tianqi_2018.csv"
    df = pd.read_csv(filepath)
    # 替换掉温度的后缀℃
    df.loc[:, "bWendu"] = df["bWendu"].str.replace("℃", "").astype('int32')
    df.loc[:, "yWendu"] = df["yWendu"].str.replace("℃", "").astype('int32')
    # print(df.head())

    # 新增一列为月份
    df['month'] = df['ymd'].str[:7]
    print(df.head())

    # 1、查看每个月的最高温度¶
    data = df.groupby('month')['bWendu'].max()
    # type(data)
    print(data)
    print("-------------------------------------------------------------")
    print(data.plot())

    # 2、查看每个月的最高温度、最低温度、平均空气质量指数¶
    group_data = df.groupby('month').agg({"bWendu":np.max, "yWendu":np.min, "aqi":np.mean})
    # print(group_data)
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    group_data.plot(ax=ax)
    fig.savefig('./pic/foo.png')

    writer = pd.ExcelWriter('./pic/savepicture.xlsx', engine='xlsxwriter')
    sheet = writer.book.add_worksheet('test')
    sheet.insert_image(0,0,'./pic/foo.png')

if __name__ == '__main__':
    # group_demo()
    # group_execute()
    group_tianqi()