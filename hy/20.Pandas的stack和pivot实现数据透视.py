import pandas as pd
import numpy as np
'''
经过统计得到多维度指标数据
使用unstack实现数据二维透视
使用pivot简化透视
stack、unstack、pivot的语法
'''
def stack_demo():
    df = pd.read_csv(
        "../datas/movielens-1m/ratings.dat",
        header=None,
        names="UserID::MovieID::Rating::Timestamp".split("::"),
        sep="::",
        engine="python"
    )
    # print(df)
    df['pdate'] = pd.to_datetime(df['Timestamp'],unit="s")
    print("-------------------------------------------------------")
    print("================",np.size)
    # res ->    按 月份、Rating  分组 统计评分的 用户 数量  => 统计 每月1-5级 评分的数量
    df_group = df.groupby([df["pdate"].dt.month, "Rating"])["UserID"].agg(pv=np.size)

    print("---------------------------------------------------")
    print(df_group)

    unstack(df_group)
# 2. 使用unstack实现数据二维透视
def unstack(df_group):
    sf_stack = df_group.unstack()
    # print(sf_stack.plot())
    print(sf_stack.stack().head(20))
    print("----------------------------------")
    # 设置 索引
    df_reset = df_group.reset_index()
    print(df_reset.head())
    df_pivot = df_reset.pivot("pdate", "Rating", "pv")
    print(df_pivot.head())

    # 总结
    # pivot方法相当于对df使用set_index创建分层索引，然后调用unstack

    # 4. stack、unstack、pivot的语法
    # stack：DataFrame.stack(level=-1, dropna=True)，将column变成index，类似把横放的书籍变成竖放
    # level=-1代表多层索引的最内层，可以通过==0、1、2指定多层索引的对应层

    # unstack：DataFrame.unstack(level=-1, fill_value=None)，将index变成column，类似把竖放的书籍变成横放
    # pivot：DataFrame.pivot(index=None, columns=None, values=None)，指定index、columns、values实现二维透视

if __name__ == '__main__':
    stack_demo()