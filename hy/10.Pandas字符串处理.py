import pandas as pd


def do_str():
    filepath = "../datas/beijing_tianqi/beijing_tianqi_2018.csv"
    df = pd.read_csv(filepath)
    print(df.head())
    print(df.dtypes)
    # 1、获取Series的str属性，使用各种字符串处理函数
    print(df["bWendu"].str)
    # 字符串替换函数
    df["bWendu"].str.replace("℃", "")
    print(df.head())
    # 判断是不是数字
    print(df['bWendu'].str.isnumeric())
    # 属性错误  AttributeError: Can only use .str accessor with string values!
    # print(df["aqi"].str.len())

    # 2、使用str的startswith、contains等得到bool的Series可以做条件查询
    condition = df['ymd'].str.startswith('2018-09')
    # print(condition)
    print(df[condition].head())

    # 3、需要多次str处理的链式操作
    # 怎样提取201803这样的数字月份？
    # 1、先将日期2018-03-31替换成20180331的形式
    # 2、提取月份字符串201803
    print(df['ymd'].str.replace('-', ''))

    # 每次调用函数，都返回一个新Series
    # AttributeError: 'Series' object has no attribute 'slice'
    # print(df["ymd"].str.replace("-", "").slice(0, 6))
    # slice 切片  截取字符串
    print(df["ymd"].str.replace("-", "").str.slice(0, 6))
    # slice就是切片语法，可以直接用
    print(df['ymd'].str.replace('-','').str[0:6])

    # 4. 使用正则表达式的处理¶
    # 添加新列
    df["中文日期"] = df.apply(get_nianyueri, axis=1)

    # 方法1：链式replace
    df["中文日期"].str.replace("年", "").str.replace("月","").str.replace("日", "")

    # Series.str默认就开启了正则表达式模式
    # 方法2：正则表达式替换
    # FutureWarning: The default value of regex will change from True to False in a future version.
    # df["中文日期"].str.replace("[年月日]", "")   
    df["中文日期"].str.replace("[年月日]", "")
    print(df.head())


def get_nianyueri(x):
    year,month,day = x["ymd"].split("-")
    return f"{year}年{month}月{day}日"



if __name__ == '__main__':
    do_str()

