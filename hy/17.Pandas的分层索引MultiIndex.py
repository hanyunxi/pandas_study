import pandas as pd


def fenceng_index():
    filepath = "../datas/stocks/互联网公司股票.xlsx"
    stocks = pd.read_excel(filepath)

    # print(stocks.shape)
    # 去重 计算 公司
    print(stocks['公司'].unique())
    # print(stocks.index)
    # 计算  公司 收盘的股票   平均价格
    print(stocks.groupby(['公司'])['收盘'].mean())

    print("-----------------------------------------------------------")
    # 计算  公司 每天 收盘的股票 平均价格
    ss = stocks.groupby(['公司', '日期'])['收盘'].mean()
    print(ss)
    print("-----------------------------------------------------------")
    # 多维索引中，空白的意思是：使用上面的值
    print(ss.shape)
    print("-----------------------------------------------------------")
    # unstack把二级索引变成列    把公司、日期变成列
    print(ss.unstack())
    print("------------------------------------------------------------")
    # 将分组的数据 设置index
    print(ss.reset_index())
    print("-------------------------------------------------------------")

    # 二、Series有多层索引MultiIndex怎样筛选数据？
    print(ss.loc['BIDU'])

    # 多层索引，可以用元组的形式筛选
    print(ss.loc['BIDU','2019-10-02'])
    print("------------------------------------------------------------")
    # 筛选 2019-10-02
    print(ss.loc[:,'2019-10-02'])

    # 三、DataFrame的多层索引MultiIndex¶
    # 设置  索引
    stocks.set_index(['公司','日期'],inplace=True)
    # 启动索引 排序
    stocks.sort_index(inplace=True)
    print(stocks.index)
    print(stocks.head(20))



    # 四、DataFrame有多层索引MultiIndex怎样筛选数据？
    # 【重要知识】在选择数据时：
    #
    # 元组(key1,key2)代表筛选多层索引，其中key1是索引第一级，key2是第二级，比如key1=JD, key2=2019-10-02
    # 列表[key1,key2]代表同一层的多个KEY，其中key1和key2是并列的同级索引，比如key1=JD, key2=BIDU
    print(stocks.loc['BIDU'])
    print("-----------------------------------------------------------")
    print(stocks.loc[('BIDU','2019-10-02'),:])
    print("-----------------------------------------------------------")
    print(stocks.loc[('BIDU', '2019-10-02'), '开盘'])
    print("-----------------------------------------------------------")
    print(stocks.loc[['BIDU', 'JD'],:])
    print("-----------------------------------------------------------")
    print(stocks.loc[(['BIDU','JD'],'2019-10-02'),:])
    print("-----------------------------------------------------------")
    print(stocks.loc[(['BIDU','JD'],'2019-10-02'),'收盘'])
    print("-----------------------------------------------------------")
    print(stocks.loc[('BIDU',['2019-10-02','2019-10-01']),'收盘'])

    # slice(None)代表筛选这一索引的所有内容
    print(stocks.loc[(slice(None), ['2019-10-02', '2019-10-03']), :])

    stocks.reset_index()
    print(stocks.head())












if __name__ == '__main__':
    fenceng_index()