import pandas as pd
import numpy as np

'''
axis=0或者"index"：
如果是单行操作，就指的是某一行
如果是聚合操作，指的是跨行cross rows
axis=1或者"columns"：
如果是单列操作，就指的是某一列
如果是聚合操作，指的是跨列cross columns
按哪个axis，就是这个axis要动起来(类似被for遍历)，其它的axis保持不动*
'''


def drop_data():
    # 将 0-11 分成 3行4列  的dataframe
    df = pd.DataFrame(
        np.arange(12).reshape(3, 4),
        columns=['A', 'B', 'C', 'D']
    )

    # 1、单列drop，就是删除某一列
    # 代表的就是删除某列
    cc = df.drop("B",axis=1)
    print(cc)
    print(df.drop("A", axis=1))

    # 2、单行drop，就是删除某一行
    # 代表的就是删除某行
    print(df.drop(1, axis=0))

    # 3、按axis=0/index执行mean聚合操作  取中位数
    # 指定了按哪个axis，就是这个axis要动起来(类似被for遍历)，其它的axis保持不动
    # axis=0 or axis=indexm   跨行输出列结果
    print(df.mean(axis=0))

    # 4、按axis=1/columns执行mean聚合操作
    # 反直觉：输出的不是每行的结果，而是每列的结果
    # 每行的  平均数   跨列输出行结果
    print(df.mean(axis=1))
    # 跨列算出 行总数
    df['sum'] = df.apply(get_sum_value,axis=1)
    print(df)
def get_sum_value(x):
    return x["A"] + x["B"] + x["C"] + x["D"]

if __name__ == '__main__':
    drop_data()
