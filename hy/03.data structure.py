import pandas as pd
import numpy as np


def dataStructure():
    # Series是一种类似于一维数组的对象，它由一组数据（不同数据类型）以及一组与之相关的数据标签（即索引）组成。
    list = [1,'a',5.2,7]
    s = pd.Series(list)
    # 获取索引
    print(s.index)
    # 获取数据
    print(s.values)
    print("-------------------------------------------------------")
    # 1.2 创建一个具有标签索引的Series
    s2 = pd.Series(list,index=['a','b','c','d'])
    print(s2)
    #1.3 使用Python字典创建Series¶
    sdata={'Ohio':35000,'Texas':72000,'Oregon':16000,'Utah':5000}
    s3 = pd.Series(sdata)
    print("-------------------------------------------------------")
    print(s3)
    print("-------------------------------------------------------")
    # 1.4 根据标签索引查询数据¶  不连续的索引  以及  连续的索引
    print(s2['b':'d'])
    print("-------------------------------------------------------")
    print(s2[['b','d']])

# 2. DataFrame
# DataFrame是一个表格型的数据结构
#
# 每列可以是不同的值类型（数值、字符串、布尔值等）
# 既有行索引index,也有列索引columns
# 可以被看做由Series组成的字典
# 创建dataframe最常用的方法，见02节读取纯文本文件、excel、mysql数据库
#
# 2.1 根据多个字典序列创建dataframe
def data_frame():
    data={
        'state':['Ohio','Ohio','Ohio','Nevada','Nevada'],
        'year':[2000,2001,2002,2001,2002],
        'pop':[1.5,1.7,3.6,2.4,2.9]
    }
    df = pd.DataFrame(data)
    # print(df)
    # 数据类型
    print(df.dtypes)
    # 数据列
    print(df.columns)
    # 数据索引
    print(df.index)

    # 3. 从DataFrame中查询出Series
    #3.1 如果只查询一行、一列，返回的是pd.Series
    print(df['year'])
    print(type(df['year']))
    print("-------------------------------------------")
    #3.2 如果查询多行、多列，返回的是pd.DataFrame
    print(df[['year', 'pop']])
    print(type(df[['year', 'pop']]))
    print("-------------------------------------------")
    # 3.3 查询一行，结果是一个pd.Series¶
    print(df.loc[0])
    print(type(df.loc[0]))
    #3.4 查询多行，结果是一个pd.DataFrame¶
    print(df.loc[1:3])
    print(type(df.loc[1:3]))


if __name__== '__main__':
    # dataStructure()
    data_frame()